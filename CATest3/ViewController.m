//
//  ViewController.m
//  CATest3
//
//  Created by Umar on 12/20/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) UIView *mainView;
@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UIView *foldView;
@property (strong, nonatomic) UISwitch *rememberSwitch;
@property (strong, nonatomic) UILabel *rememberLabel;
@property (strong, nonatomic) UILabel *securityLabel;
@property (strong, nonatomic) UITextField *entryField;
@property (strong, nonatomic) UIButton *submitButton;

@end

@implementation ViewController
@synthesize mainView,headerView, foldView, rememberSwitch, rememberLabel, securityLabel, entryField, submitButton;

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self layoutStack];
}

-(void)awakeFromNib{
    CATransform3D perspectiveTransform = CATransform3DIdentity;
    perspectiveTransform.m34 = 1/-900.0;
    self.view.layer.sublayerTransform = perspectiveTransform;
    mainView.layer.sublayerTransform = perspectiveTransform;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 280, 188)];
    mainView.backgroundColor = [UIColor whiteColor];
    mainView.center = self.view.center;
    mainView.layer.cornerRadius = 8;
    mainView.layer.shadowRadius = 8;
    mainView.layer.shadowOpacity = .7;
    mainView.layer.shadowOffset = CGSizeMake(0, 2);
    
    headerView = [[UIView alloc]initWithFrame:CGRectMake(21, 10, 237, 45)];
    
    headerView.layer.borderWidth = 1;
    headerView.layer.cornerRadius = 4;
    
    foldView = [[UIView alloc]initWithFrame:CGRectMake(21, 14, 237, 79)];
    
    foldView.layer.borderWidth = 1;
    foldView.layer.cornerRadius = 4;
    foldView.layer.doubleSided = false;
    foldView.layer.anchorPoint = CGPointMake(.5, 0);
    
    [self.view addSubview:mainView];
    [mainView addSubview:headerView];
    [mainView addSubview:foldView];
    
    
    rememberSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(8, 7, 51, 31)];
    rememberSwitch.on = YES;
    [rememberSwitch addTarget:self action:@selector(valueChanges:) forControlEvents:UIControlEventValueChanged];
    
    rememberLabel = [[UILabel alloc]initWithFrame:CGRectMake(65, 11, 117, 24)];
    [rememberLabel setFont:[UIFont fontWithName:@"Avenir-Bold" size:17]];
    [rememberLabel setText:@"Remember me"];
    
    [headerView addSubview:rememberSwitch];
    [headerView addSubview:rememberLabel];
    
    
    entryField = [[UITextField alloc]initWithFrame:CGRectMake(0, 50, 237, 30)];
    entryField.placeholder = @"(555) 123-1234";
    [self layoutStack];
    
    entryField.layer.borderWidth = 1;
    entryField.layer.cornerRadius = 4;
    entryField.delegate = self;
    
    securityLabel = [[UILabel alloc]initWithFrame:CGRectMake(8,8,221,41)];
    securityLabel.attributedText = [[NSAttributedString alloc]initWithString:@"For security, please enter your phone number" attributes:@{                                                NSFontAttributeName: [UIFont fontWithName:@"Avenir" size:15.0]
     }];
    
    
    [foldView addSubview:entryField];
    [foldView addSubview:securityLabel];
    
    submitButton = [[UIButton alloc]initWithFrame:CGRectMake(21, 144, 237, 33)];
    submitButton.layer.cornerRadius = 4;
    [submitButton setBackgroundColor:[UIColor colorWithRed:92.0/255.0 green:154.0/255.0 blue:231.0/255.0 alpha:1.0]];
    [submitButton setTitle:@"Pay $25.00" forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [mainView addSubview:submitButton];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)submitButtonPressed:(UIButton *)button{
    if (entryField.text.length > 0) {
        [self animateButton:button title:@"Sending..." color:[UIColor grayColor]];
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            // code to be executed on the main queue after delay
            [self animateButton:button title:@"Sent!" color:[UIColor greenColor]];
        });
    }else{
        CAKeyframeAnimation *translation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
        translation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
                translation.values = @[
                                         [NSNumber numberWithInt:-5]
                                        ,[NSNumber numberWithInt:5]
                                        ,[NSNumber numberWithInt:-5]
                                        ,[NSNumber numberWithInt:5]
                                        ,[NSNumber numberWithInt:-3]
                                        ,[NSNumber numberWithInt:3]
                                        ,[NSNumber numberWithInt:-2]
                                        ,[NSNumber numberWithInt:2]
                                        ,[NSNumber numberWithInt:0]];
        
        CAKeyframeAnimation *rotation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.y"];
        
        rotation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];


        rotation.values = @[[NSNumber numberWithInt:[self toRadian:-5]]
                           ,[NSNumber numberWithInt:[self toRadian:5]]
                           ,[NSNumber numberWithInt:[self toRadian:-5]]
                           ,[NSNumber numberWithInt:[self toRadian:5]]
                           ,[NSNumber numberWithInt:[self toRadian:-3]]
                           ,[NSNumber numberWithInt:[self toRadian:3]]
                           ,[NSNumber numberWithInt:[self toRadian:-2]]
                           ,[NSNumber numberWithInt:[self toRadian:2]]
                           ,[NSNumber numberWithInt:[self toRadian:0]]
                           ];
        
        CAAnimationGroup *shakeGroup = [CAAnimationGroup animation];
        shakeGroup.animations = @[translation, rotation];
        shakeGroup.duration = .6;
        [mainView.layer addAnimation:shakeGroup forKey:@"shakeIt"];


        
    }
    
}


-(CGRect)foldFrame:(CGFloat)top{
    return CGRectMake(self.headerView.frame.origin.x, top, self.headerView.bounds.size.width, 80);
}

- (void)valueChanges:(UISwitch *)sender {
    [UIView animateWithDuration:.7 delay:0 usingSpringWithDamping:.7 initialSpringVelocity:.2 options: UIViewAnimationOptionAllowAnimatedContent animations:^{
        int angle = !sender.on ? -90 : 0;
        self.foldView.layer.transform = CATransform3DMakeRotation([self toRadian:angle], 1.0, 0, 0);
        [self layoutStack];
        //        self.mainView.frame = self.headerView.frame ;//[self foldFrame:CGRectGetMaxY(self.headerView.frame)-self.foldView.layer.borderWidth];
                self.mainView.frame = CGRectMake(self.mainView.frame.origin.x, self.mainView.frame.origin.y, self.mainView.frame.size.width, CGRectGetMaxY(self.submitButton.frame)+10);
        // CGRectGetMaxY(self.submitButton.frame)+10;
        
        
    }completion:nil];
}

-(void)animateButton:(UIButton *)button title:(NSString *)title color:(UIColor *)color{
    CATransition *transition = [CATransition new];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionReveal;
    transition.duration = .25;
    [button.titleLabel.layer addAnimation:transition forKey:kCATransition];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    
    
}

-(void)layoutStack{
    CGFloat margin = 10;
    self.foldView.frame = [self foldFrame:CGRectGetMaxY(self.headerView.frame)-self.foldView.layer.borderWidth];
    self.submitButton.frame = CGRectMake(self.submitButton.frame.origin.x, (CGRectGetMaxY(self.foldView.frame))+margin, self.submitButton.frame.size.width, self.submitButton.frame.size.height);
}


-(CGFloat)toRadian:(int)value{
    
    return (CGFloat)(((double)value/180.0)*M_PI);
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


@end
