//
//  AppDelegate.h
//  CATest3
//
//  Created by Umar on 12/20/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

